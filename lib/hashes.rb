# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  results = {}
  str.split(" ").each {|word| results[word] = word.length}
  results
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k, v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k, v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  results = Hash.new(0)
  word.each_char {|char| results[char] += 1}
  results
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  unique_hash = {}
  arr.each {|i| unique_hash[i] = 0}
  unique_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count_hash = {even: 0, odd: 0}
  numbers.each {|i| i.odd? ? count_hash[:odd] += 1 : count_hash[:even] += 1}
  count_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  count_hash = Hash.new(0)
  string.downcase.gsub(/[^aeiou]/, "").each_char {|vowel| count_hash[vowel] += 1}
  arr = count_hash.sort_by {|k, v| v}.flatten
  arr[arr.index(arr[-1]) - 1]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  birthdays = []
  students.each {|k, v| birthdays << k if v >= 7}
  birthdays.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_count = Hash.new(0)
  specimens.each {|species| species_count[species] += 1}
  species_arr = species_count.sort_by {|k, v| v}
  (species_count.length ** 2) * species_arr[0][1] / species_arr[-1][1]
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_count = Hash.new(0)
  vandalized_sign_count = Hash.new(0)
  normal_sign.downcase.gsub(/[^a-z]/, "").each_char {|char| normal_sign_count[char] += 1}
  vandalized_sign.downcase.gsub(/[^a-z]/, "").each_char {|char| vandalized_sign_count[char] += 1}
  vandalized_sign_count.each {|k, v| return false if normal_sign_count[k] < vandalized_sign_count[k]}
  true
end